package com.example.controllers;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.LinkedHashMap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.example.controller.ClientUserController;
import com.example.model.ClientUser;
import com.example.service.ClientUserService;
import com.fasterxml.jackson.databind.ObjectMapper;


@SpringBootTest
public class CControllerUnitTesting {

	@Mock
	private ClientUserService clServ;
	
	@InjectMocks
	private ClientUserController clController;
	
	private ClientUser cu;	
	
	private MockMvc mock;
	

	LinkedHashMap<String, String> loginInfo = new LinkedHashMap<>();
	@BeforeEach
	public void setUp() throws Exception {
		cu = new ClientUser("Star", "Johnson", "Space", 55, "starjohnson@maildrop.cc", "5015558888",
				"starjohnson", "password", null, null);
		mock = MockMvcBuilders.standaloneSetup(clController).build();

		when(clServ.verify("starjohnson", "password")).thenReturn(true);
		when(clServ.verify("starjohnson2", "nonsense")).thenReturn(false);
		doNothing().when(clServ).sendPasswordEmail("starjohnson@mailrop.cc");
		
	}
	
	
	@Test
	public void verifyLoginSuccess() throws  Exception {
		loginInfo.put("username","starjohnson" );
		loginInfo.put("password","password");
		mock.perform(post("/api/users/login").contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(loginInfo)))
		.andExpect(status().isAccepted());
	}
	@Test
	public void verifyLoginFailure() throws Exception {
		loginInfo.put("username","starjohnson" );
		loginInfo.put("password","nonsense");
		mock.perform(post("/api/users/login").contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(loginInfo)))
		.andExpect(status().isBadRequest());
		
	}
	
	@Test
	public void getPasswordTest() throws Exception {
		loginInfo.put("email", "starjohnson@maildrop.cc");
		mock.perform(post("/api/users/forgotpassword").contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(loginInfo)))
		.andExpect(status().isAccepted()).andExpect(jsonPath("$").value("Email has been sent"));
		
	}
	@Test
	public void updateTokenTest() throws Exception {
		loginInfo.put("username","starjohnson" );
		loginInfo.put("password","password");
		mock.perform(post("/api/users/login").contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(loginInfo)))
		.andExpect(status().isAccepted());

	}
	
}
