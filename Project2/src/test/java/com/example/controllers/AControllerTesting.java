package com.example.controllers;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.example.controller.AccountController;
import com.example.model.Account;
import com.example.model.AccountType;
import com.example.model.Branch;
import com.example.model.ClientUser;
import com.example.model.Transaction;
import com.example.service.AccountService;
import com.example.service.AccountTypeService;
import com.example.service.BranchService;
import com.example.service.ClientUserService;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
public class AControllerTesting {

	
	@Mock
	private AccountService aServ;
	
	@Mock
	private ClientUserService cuServ;
	
	@Mock
	private BranchService bServ;
	
	@Mock
	private AccountTypeService acTypeServ;
	
	@InjectMocks
	private AccountController acController;
	
	private Account ac;
	private Account ac2;
	
	private ClientUser cu;

	
	private MockMvc mock;
	
	List<Transaction> t = new ArrayList<>();
	
	LinkedHashMap<String, String> transferfundinfo = new LinkedHashMap<>();
	@BeforeEach
	public void setUp() throws Exception{		
		ClientUser c = new ClientUser("Star", "Johnson", "Space", 55, "starjohnson@maildrop.cc", "5015558888",
				"starjohnson", "password", null, null);
		AccountType at1 = new AccountType("checking");
		AccountType at2 = new AccountType("savings");
		Branch b1 = new Branch("Santa Clara");
		Branch b3 = new Branch("San Francisco");
		ac  = new Account("132548101", 5, c, at1, b1, t); // not auto generated for account no yet.
		double deposit = 200;
		ac2 =  new Account("132548102", 10000, c, at2, b3, t);
		double currBalance = ac2.getBalance() + deposit;
		ac2.setBalance(currBalance);
		Transaction t1 = new Transaction("Payday", new Timestamp(System.currentTimeMillis()), 0, 200, currBalance, ac2);
		double withdrawal = 280;
		currBalance = ac2.getBalance() - withdrawal;
		ac2.setBalance(currBalance);
		Transaction t2 = new Transaction("Purchase of Amazing Figure", new Timestamp(System.currentTimeMillis()), 280,
				0, currBalance, ac2);
		t.add(t1);
		t.add(t2);

		mock = MockMvcBuilders.standaloneSetup(acController).build();
		doNothing().when(aServ).transferFunds(ac.getAccountNumber(), ac2.getAccountNumber(), .02);
		doNothing().when(aServ).insertAccount(ac);
 
	}
	@Test
	public void fundTransferSuccess() throws Exception {
		transferfundinfo.put("acc1", ac.getAccountNumber());
		transferfundinfo.put("acc2", ac2.getAccountNumber());
		transferfundinfo.put("amount", ".02");
		mock.perform(post("/api/bank/transfer").contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(transferfundinfo)))
		.andExpect(status().isAccepted());
	}
	
	@Test
	public void createNewAccountSuccess() throws Exception {
		Map<String, String> accountMap = new LinkedHashMap<>();
		accountMap.put("accountType", "1");
		accountMap.put("branch", "1");
		accountMap.put("clientId", "3");
		mock.perform(post("/api/bank/profile/createaccount").contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(accountMap)))
		.andExpect(status().isCreated()).andExpect(jsonPath("$").value("new account created"));
	}
}
