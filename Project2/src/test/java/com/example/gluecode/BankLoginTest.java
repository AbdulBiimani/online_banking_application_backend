package com.example.gluecode;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.example.e2e.page.BankLogin;
//import com.example.e2e.page.BankRegistration;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class BankLoginTest {
public BankLogin bl;


@Given("a user is at the login page of bank application ready to login")
public void a_user_is_at_the_login_page_of_bank_application() {
    // Write code here that turns the phrase above into concrete actions
	this.bl = new BankLogin(BankDriverUtility.driver);
	assertEquals(BankLogin.title, BankDriverUtility.driver.getTitle());
}
@When("a a user inputs their username {string}")
public void a_a_user_inputs_username(String string) {
	this.bl.username.sendKeys(string);
}


@When("then a user inputs their password  {string}")
public void then_a_user_inputs_their_password(String string) {
    // Write code here that turns the phrase above into concrete actions
    this.bl.password.sendKeys(string);
}

@When("then submits the login information")
public void then_submits_the_information() {
    // Write code here that turns the phrase above into concrete actions
    this.bl.loginButton.click();
    System.out.println(this.bl.loginButton.getText());
}


@Then("the user is redirected to the bank page")
public void the_user_is_redirected_to_the_bank_page() {
		WebDriverWait wait = new WebDriverWait(BankDriverUtility.driver, 60);
	    wait.until(ExpectedConditions.urlMatches("http://localhost:4200/bank"));
	    assertEquals("http://localhost:4200/bank", BankDriverUtility.driver.getCurrentUrl());
}

}
