package com.example.gluecode;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.example.e2e.page.Bank;
import com.example.e2e.page.BankLogin;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ProfileHomeTest {
	public BankLogin bl;
	public Bank b;
	@Given("a user is logged in at the bank page wanting to go to profile with {string} {string}")
	public void a_user_is_logged_in_at_the_bank_page_wanting_to_go_to_profile_with(String string, String string2) {
		bl = new BankLogin(BankDriverUtility.driver);
		b = new Bank(BankDriverUtility.driver);
		bl.username.sendKeys(string);
		bl.password.sendKeys(string2);
		bl.loginButton.click();
		WebDriverWait wait = new WebDriverWait(BankDriverUtility.driver, 6);
	    wait.until(ExpectedConditions.urlMatches("http://localhost:4200/bank"));

		
	}
	
	@When("a user clicks the profile link")
	public void a_user_clicks_the_profile_link() {
		b.profile.click();
		System.out.println(this.b.profile.getText());
		
	}
	@Then("they are redirected to the profile page")
	public void they_are_redirected_to_the_profile_page() {
		WebDriverWait wait = new WebDriverWait(BankDriverUtility.driver, 60);
	    wait.until(ExpectedConditions.urlMatches("http://localhost:4200/bank/profile"));
	    assertEquals("http://localhost:4200/bank/profile", BankDriverUtility.driver.getCurrentUrl());
	}
	

	
}
