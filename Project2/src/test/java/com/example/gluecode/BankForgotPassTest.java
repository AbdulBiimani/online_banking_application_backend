package com.example.gluecode;

import com.example.e2e.page.BankForgotPass;
import com.example.e2e.page.BankLogin;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class BankForgotPassTest {
	public BankForgotPass bfp;
	public BankLogin bl;
	@Given("a user is at the login page of bank application ready to click forgot password")
	public void a_user_is_at_the_login_page_of_bank_application_ready_to_click_forgot_password() {
	    this.bfp = new BankForgotPass(BankDriverUtility.driver);
	    this.bl = new BankLogin(BankDriverUtility.driver);
	}
	
	@When("a user click forgotpassword button")
	public void a_user_click_forgotpassword_button() {
//	   this.bfp.fButton.click();
		this.bl.fPassButton.click();
	}
	
	@When("then a user inputs a email {string}")
	public void then_a_user_inputs_email(String string) {
		this.bfp.email.sendKeys(string);
	}
	@Then("submits the email information")
	public void submits_the_email_information() {
	   this.bfp.fButton.click();
	}

	
	

}
