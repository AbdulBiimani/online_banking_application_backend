package com.example.e2e.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BankLogin {
	
	public static final String title = "OnlineBanking";
	
	@FindBy(name="username")
	public WebElement username;
	
	@FindBy(xpath = "//input[@id= 'password']")
	public WebElement password;
	
	@FindBy(xpath = "//a[@id='register-link']")
	public WebElement registerLink;
	
	@FindBy(xpath = "/html/body/app-root/app-user/div[2]/div/app-login/div/div/form/div[3]/button")
	public WebElement loginButton;
	@FindBy(xpath = "//a[@id='forgotpassword']")
	public WebElement fPassButton;
//	@FindBy(linkText = "Home")
//	public WebElement homeLink;
	
//	@FindBy(xpath = "//a[text()='SIGN-ON']")
//	public WebElement signOnLink;

	

	public void login()
	{
		this.loginButton.click();
	}
	public BankLogin(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	
}
