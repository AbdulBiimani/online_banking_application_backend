package com.example.e2e.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class CreateAccount {

	@FindBy(xpath = "//*[@id='type']")
	public WebElement accounttype;
	@FindBy(xpath = "//*[@id='branch']")
	public WebElement branch;
	@FindBy(xpath = "/html/body/app-root/app-bank/div/div/app-profile/app-account-create/div/div/form/div[5]/button")
	public WebElement subButton;
	public CreateAccount(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	public Select getSelectOptions1() {
		return new Select(accounttype);
	}

	public Select getSelectOptions2() {
		return new Select(branch);
	}


}
