package com.example.e2e.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UpdateProfile {

	@FindBy(xpath = "//input[@formcontrolname= 'firstName']")
	public WebElement firstName;
	
	@FindBy(xpath = "//input[@formcontrolname= 'lastName']")
	public WebElement lastName;
	
	@FindBy(xpath = "//input[@formcontrolname= 'contactNumber']")
	public WebElement contactNumber;
	
	@FindBy(xpath = "//input[@formcontrolname= 'address']")
	public WebElement address;
	@FindBy(xpath = "/html/body/app-root/app-bank/div/div/app-profile/app-profile-update/div/div/form/div[6]/button")
	public WebElement subButton;
	@FindBy(xpath = "//input[@formcontrolname= 'email']")
	public WebElement email;
	public UpdateProfile(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
