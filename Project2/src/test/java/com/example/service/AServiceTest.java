package com.example.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.model.Account;
import com.example.model.AccountType;
import com.example.model.Branch;
import com.example.model.ClientUser;
import com.example.model.Transaction;
import com.example.repository.AccountRepository;
@SpringBootTest
public class AServiceTest {

	@Mock
	private AccountRepository aRepo;
	
	@Mock
	private TransactionService tService ;

	@InjectMocks
	private AccountService aService;
	List<Transaction> tList = new ArrayList<>();
	
	private AccountService accServ = mock(AccountService.class);

	private Account a;
	@BeforeEach
	public void Setup() throws Exception{
		ClientUser c = new ClientUser( "Star", "Johnson", "Space", 55, "starjohnson@maildrop.cc", "5015558888",
				"starjohnson", "password", null, null);
		Branch b3 = new Branch("San Francisco");
		AccountType at2 = new AccountType("savings");
		AccountType at1 = new AccountType("checking");
		Branch b1 = new Branch("Santa Clara");
		 a = new Account("132548102", 10000, c, at2, b3, tList); // not auto generated for account no yet. //
		Timestamp tstmp = Timestamp.valueOf("2021-01-26 10:00:34");
		Transaction t1 = new Transaction(4,"Transfer of money from savings to checking",tstmp, 0, 5000.02, 5005.02, a );
		Account a3 = new Account("132548101", 5, c, at1, b1, tList); // not auto generated for account no yet.
		tList.add(t1);
		when(aRepo.findByAccountNumber("132548102")).thenReturn(a);
		when(aRepo.findByAccountNumber("132548101")).thenReturn(a3);
	}
	
	@Test
	public void getAccountByNumberSuccess() {
		
		assertEquals(aService.getAccountByAccountNumber("132548102"), a);
	}
	@Test
	public void verifyFundTransferSuccess() {
		doNothing().when(accServ).transferFunds("132548102", "132548101", .02);
		accServ.transferFunds("132548102", "132548101", .02);
		verify(accServ, times(1)).transferFunds("132548102", "132548101", .02);
	}
	
	@Test
	public void testInsertNewAccountSuccess() {
		doNothing().when(accServ).insertAccount(a);
		accServ.insertAccount(a);
		verify(accServ, times(1)).insertAccount(a);
	}
}
