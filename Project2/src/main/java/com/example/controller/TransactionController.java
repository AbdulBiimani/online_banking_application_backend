package com.example.controller;

import java.util.LinkedHashMap;
import java.util.List;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.mailutils.JavaMailUtils;
import com.example.model.Transaction;
import com.example.service.TransactionService;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@RestController
@RequestMapping(value="/api/transaction")
@AllArgsConstructor(onConstructor=@__(@Autowired))
@NoArgsConstructor
//@CrossOrigin(origins = "http://localhost:4200")
//@CrossOrigin(origins = "http://revatureproject2onlinebankingwebsite.s3-website.us-east-2.amazonaws.com")
@CrossOrigin(origins = "*")

public class TransactionController {
	
	TransactionService transactionService;
	
	
	@GetMapping("/view")
	public ResponseEntity<List<Transaction>> viewTransaction(@RequestParam("account") String account) {
		List<Transaction> tList = transactionService.getTransactionByAccount(account);
		if(tList== null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(tList, HttpStatus.OK);
	}
	
	@PostMapping(value="/deposit")
	public ResponseEntity<String> postDeposit(@RequestBody LinkedHashMap<String,String> depositlink) {
		System.out.println(depositlink);
		String msg = transactionService.deposit(depositlink.get("account_number"), Double.parseDouble(depositlink.get("creditAmount")),depositlink.get("description"));
		if(msg.equals("Transaction succsessful"))
			return new ResponseEntity<>(msg,HttpStatus.ACCEPTED);
			else
			return new ResponseEntity<>(msg,HttpStatus.BAD_REQUEST);
	}
	
	@PostMapping(value="/withdraw")
	public  ResponseEntity<String> postWithdraw(@RequestBody LinkedHashMap<String,String> withdrawlink) {
		String msg = transactionService.withdraw(withdrawlink.get("account_number"), Double.parseDouble(withdrawlink.get("debitAmount")),withdrawlink.get("description"));
		if(msg.equals("Transaction succsessful"))
		return new ResponseEntity<>(msg,HttpStatus.ACCEPTED);
		else
		return new ResponseEntity<>(msg,HttpStatus.BAD_REQUEST);
	}
	@PostMapping(value="/delete")
	public  ResponseEntity<String> postClose(@RequestBody LinkedHashMap<String,String> closelink) {
		System.out.println(closelink);

		String email = closelink.get("email");

		String msg = transactionService.closeAccount(closelink.get("account_number"));
		if(msg.equals("Account successfully deleted")) {
			try {
				JavaMailUtils.sendAsHtml(email, "account successfully deleted", "<p>Your account was successfully deleted</p>");
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return new ResponseEntity<>(msg,HttpStatus.ACCEPTED);
		}
		else
		return new ResponseEntity<>(msg,HttpStatus.BAD_REQUEST);
	}
	
	
}
