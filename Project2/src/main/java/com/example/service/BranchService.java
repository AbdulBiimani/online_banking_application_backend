package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.Branch;
import com.example.repository.BranchRepository;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
@NoArgsConstructor

public class BranchService {

	public BranchRepository branchRepository;
	public Branch getById(int branchId) {
		Branch branch = branchRepository.findByBranchId(branchId);
		return branch;
	}

}
