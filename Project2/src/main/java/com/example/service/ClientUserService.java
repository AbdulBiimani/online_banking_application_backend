package com.example.service;

import java.util.List;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.mailutils.JavaMailUtils;
import com.example.model.ClientUser;
import com.example.repository.ClientUserRepository;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Service
@AllArgsConstructor(onConstructor=@__(@Autowired))
@NoArgsConstructor 
public class ClientUserService {
	
	private ClientUserRepository clientUserRepository;
	
	public ClientUser getClientByUsername(String username) {	
		return clientUserRepository.findByUsername(username);
	}
	
	public List<ClientUser> getAllClient(){
		return clientUserRepository.findAll();
	}
	
	public ClientUser insertClient(ClientUser user) {
		clientUserRepository.save(user);
		return user;
	}
	
	public void updateClient(ClientUser client) {
		clientUserRepository.save(client);
//		return client;
	}

	public boolean verify(String username, String password) {
		boolean isVerified = false;
		ClientUser cUser = getClientByUsername(username);
//		System.out.println(cUser);
//		System.out.println(cUser.getPassword());
		if(cUser == null)
		{
			return isVerified;
		}
		if(cUser.getPassword().equals(password))
		{
			System.out.println("login verified");
			isVerified = true;
		}
		return isVerified;
	}

	public ClientUser getClientByClientId(int clientId) {
		ClientUser cu = clientUserRepository.findByclientId(clientId);
		return cu;
	}
	public void sendPasswordEmail(String email) {
			
		ClientUser cu = clientUserRepository.findByEmail(email);
		
//		System.out.println(cu);
		
		try {
			JavaMailUtils.sendAsHtml(cu.getEmail(), "Forgotten Password", "<h2>Forgotten Password</h2><p> Hello " + cu.getFirstName() + ". Your password is " + cu.getPassword() + "</p>\"");
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}