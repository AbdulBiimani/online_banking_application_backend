Feature: Bank Application Profile Home
	As A user I wish to get to the profile home page
	 
	Scenario Outline: Get To Profile Home
		Given a user is logged in at the bank page wanting to go to profile with "<username>" "<password>"
		When a user clicks the profile link
		Then they are redirected to the profile page

	Examples:
		| username     | password  |
		| johnlao      | password  |
		| leooo        | password  | 
		| starjohnson  | password  |