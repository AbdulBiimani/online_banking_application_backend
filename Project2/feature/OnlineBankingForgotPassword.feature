Feature: Bank Application Forgot Password
	As a User, I wish to be able to get password
	
	Scenario Outline: Get A Password Email
		Given a user is at the login page of bank application ready to click forgot password
		When a user click forgotpassword button
		And then a user inputs a email "<email>"
		Then  submits the email information

	Examples:
		| email                      | 
		| starjohnson@maildrop.cc    |
		| henryzheng@maildrop.cc     |
		| alexhuang@maildrop.cc      |