Feature: Bank Application Withdrawal
	As A user I wish to withdraw from an account
	
		Scenario Outline: Make a Withdrawal
		Given a user is logged in at the bank page wanting to go to withdrawal with "<username>" "<password>"
		When a user clicks the withdrawal link
		And they are redirected to the withdrawal page
		And they select an account number for withdrawal "<accountNum>"
		And they input a withdrawal amount "<amount>"
		And they input a withdrawal description "<description">
		Then the user submits the withdrawal
	Examples:
		| username     | password  |  accountNum| amount |description| 
		| starjohnson  | password  |132548102   |  15   |paycheck|
		| johnlao      | password  |0300001001  | 10   |paydat|
		| leooo | password  |0300002001  | 10    |paydat|